Feature: Pet store

  Scenario Outline: User can add pet to store
    When I add pet <name> to store
    Then I get status code 200
    And I can find last created pet at store

    Examples:
      | name     | scenario     |
      | "mr_bob" | default name |
      | ""       | empty name   |

  Scenario Outline: User can update pet in store
    Given I add pet "to_modify" to store

    When I update pet name <name> at store
    Then I get status code 200
    And I can find last updated pet at store

    Examples:
      | name        | scenario  |
      | "new_name"  | new name  |
      | "to_modify" | same name |

  Scenario: User can not delete not existing pet
    Given I add pet "test_pet" to store

    When I delete pet id 999999 from store
    Then I get status code 404
