package petstore.test.steps;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import petstore.test.ScenarioContext;

@Slf4j
public class BaseStep {

    @Autowired
    protected ScenarioContext scenarioContext;

}
