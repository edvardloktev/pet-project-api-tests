package petstore.test.steps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import petstore.test.api.models.Pet;
import petstore.test.api.rest.PetAPI;
import petstore.test.factory.PetFactory;


@Slf4j

public class PetStep extends BaseStep {

    @Autowired
    private PetAPI petApi;


    @When("I add pet {string} to store")
    public void addPetToStore(String petName) {
        Pet pet = PetFactory.next(petName);
        Response resp = petApi.createPet(pet);
        scenarioContext.setResponse(resp);
    }

    @When("I delete pet id {int} from store")
    public void deletePetFromStore(int petId) {
        Response resp = petApi.deletePet(petId);
        scenarioContext.setResponse(resp);
    }

    @When("I update pet name {string} at store")
    public void updatePetFromStore(String petName) {
        Pet petFromLastCallToBeUpdated = scenarioContext.getResponse().getBody().as(Pet.class);
        petFromLastCallToBeUpdated.setName(petName);

        Response resp = petApi.updatePet(petFromLastCallToBeUpdated);
        scenarioContext.setResponse(resp);
    }

    @Then("I can find last (created|updated) pet at store$")
    public void isPetOnStore(String ignored) {
        Pet petFromLastCall = scenarioContext.getResponse().getBody().as(Pet.class);
        Response resp = petApi.getPetById(petFromLastCall.getId());
        scenarioContext.setResponse(resp);

        assert resp.getBody().as(Pet.class).equals(petFromLastCall);
    }

}