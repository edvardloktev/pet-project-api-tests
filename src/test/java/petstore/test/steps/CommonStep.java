package petstore.test.steps;

import io.cucumber.java.en.Then;

public class CommonStep extends BaseStep{
    @Then("I get status code {int}")
    public void gotStatus(int code) {
        assert scenarioContext.getResponse().getStatusCode() == code;
    }
}
