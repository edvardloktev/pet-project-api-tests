package petstore.test.factory;

import com.github.javafaker.Faker;
import petstore.test.api.models.Category;
import petstore.test.api.models.Pet;
import petstore.test.api.models.Status;
import petstore.test.api.models.Tag;

import java.util.Arrays;

public class PetFactory {

    private static final Faker faker = new Faker();

    public static Pet next(String petName) {
        return Pet.builder()
                .id(faker.random().nextInt(10000, 99999))
                .name(petName)
                .photoUrls(Arrays.asList(faker.internet().url()))
                .category(new Category(faker.random().nextInt(10000, 99999), faker.animal().name()))
                .tags(Arrays.asList(new Tag(faker.random().nextInt(10000, 99999), faker.animal().name())))
                .status(Status.NEW)
                .build();
    }

}
