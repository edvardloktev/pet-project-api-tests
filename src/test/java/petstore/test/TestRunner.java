package petstore.test;


import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.DataProvider;

@CucumberOptions(
        features = "classpath:features",
        plugin = {
                "pretty",
                "json:target/cucumber-report.json",
                "html:target/cucumber-report.html",
                "testng:target/testng-results.xml"
        },
        tags = "not @wip")
public class TestRunner extends AbstractTestNGCucumberTests {

    @Override
    @DataProvider
    public Object[][] scenarios() {
        return super.scenarios();
    }
}