package petstore.test.api.models;

public enum Status {
    NEW, LOST
}
