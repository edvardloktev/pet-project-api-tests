package petstore.test.api.rest;

import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import petstore.test.api.models.Pet;


import static io.restassured.RestAssured.given;

@Slf4j
@Component
public class PetAPI extends BaseAPI {

    private static final String URI = "/pet";

    public Response createPet(Pet newPet) {
        return given(requestSpecification)
                .body(newPet)
                .when()
                .post(URI)
                .then()
                .log()
                .body()
                .extract()
                .response();
    }

    public Response updatePet(Pet updatedPet) {
        return given(requestSpecification)
                .body(updatedPet)
                .when()
                .put(URI)
                .then()
                .log()
                .body()
                .extract()
                .response();
    }

    public Response deletePet(int id) {
        return given(requestSpecification)
                .when()
                .delete(URI + "/" + id)
                .then()
                .log()
                .body()
                .extract()
                .response();
    }

    public Response getPetById(int id) {
        return given(requestSpecification)
                .when()
                .get(URI + "/" + id)
                .then()
                .log()
                .body()
                .extract()
                .response();
    }
}
