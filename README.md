# petstore-rest-assured

The testing framework for petstore service: https://petstore.swagger.io/

### Tools
Used tools/frameworks:
* Spring
* REST Assured
* Lombok

### Run
Run tests:

- `mvn clean test`


### Test reports
Cucumber html report generated at `.\target\cucumber-report.html`
